/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td1;

/**
 *
 * @author gabetb
 */
public class Eleve{
    String nom, prenom;
    public Eleve(String nom, String prenom){
        this.nom = nom;
        this.prenom = prenom;
    }
    
    public void hello(){
        System.out.println("Bonjour " + this.prenom + " " + this.nom);
    }
}
