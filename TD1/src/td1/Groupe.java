/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package td1;

/**
 *
 * @author gabetb
 */
public class Groupe {
    int groupe;
    int effectif;
    Eleve[] classe;
    
    public Groupe(int groupe, Eleve[] classe){
        this.groupe = groupe;
        this.effectif = classe.length;
        this.classe = new Eleve[this.effectif];
        for(int indice = 0; indice<this.effectif; indice++)
        {
            this.classe[indice] = classe[indice];
        }
    }
    
    public void AfficheGroupe(){
        for(int indice = 0; indice<this.effectif; indice++)
        {
            System.out.println(classe[indice].nom + " " + classe[indice].prenom + " du groupe: " + this.groupe);
        }
    }    
    
}
